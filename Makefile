FIGURES=$(wildcard Figures/ch*)

all: final-run

first-run: mybibliography.bib
	mkdir -p build
	lualatex thesis.tex
	mv *.aux *bcf build
	rm *.lof *.log *.lot *.out *.xml *.toc

biber: first-run
	biber build/thesis

final-run: thesis.tex biber $(FIGURES)
	lualatex -output-directory build $<
	lualatex -output-directory build $<
	open build/thesis.pdf

.PHONY: clean all
clean:
	rm -r build
	rm *.lof *.log *.lot *.out *.xml *.toc *.pdf *.bbl *.bcf *.blg


