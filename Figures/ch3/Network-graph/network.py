import numpy as np
import os,glob,subprocess

header = r'''\documentclass[tikz, border=1pt]{standalone}
\usetikzlibrary{shapes}
\begin{document}
\begin{tikzpicture}
'''

footer = r'''
\end{tikzpicture}
\end{document}'''

main = ""

y_scale = 1.5
x_scale = 2.4 * y_scale
N = 1

main += r"\node[opacity=0.6] (0, 0, 0) at ({},{})".format(-0.16,-0.6 * y_scale) + '''{(0, 0, 0)};
'''

for i in range(1, 8):
    N += 1 if (i % 2 == 0) else 0
    x_offset = 0.5 if (i % 2 != 0) else 0
    for j in range(N):
        coordinates = ((x_offset + j) * x_scale, - y_scale * i)
        phyllo = (i, i - (N - 1 - j), abs(j - (N - 1)))

        if (phyllo[2] == 0 and phyllo[1] > 2):
            colour = "cyan!6"
        elif (phyllo[2] == 1 and phyllo[1] > 2):
            colour = "red!6"
        elif (phyllo[2] == 2 and phyllo[1] > 3):
            colour = "cyan!6"
        elif (phyllo[2] == phyllo[1] - 1 and phyllo[1] > 1):
            colour = "black!6"
        elif (phyllo[2] == phyllo[1] and phyllo[1] > 1):
            colour = "orange!6"
        else:
            colour = "white"

        main += r"\node[fill={}, draw={}, ellipse] ({}{}{}) ".format(colour, colour, *phyllo) + "at ({}, {})".format(*coordinates) + r"{{\Large$({}, {}, {})$}}".format(*phyllo) + r''';
        '''

main += r"\draw [dashed, line width=0.3, color=gray, domain=-55:-95] plot ({{5 * {:.3f} * cos(\x)}}, {{5 * sin(\x)}});".format(x_scale / y_scale)
main += r"\draw [dashed, line width=0.3, color=gray, domain=-56:-94] plot ({{6.7 * {:.3f} * cos(\x)}}, {{6.7 * sin(\x)}});".format(x_scale / y_scale)
main += r"\draw [dashed, line width=0.3, color=gray, domain=-57:-93] plot ({{8.35 * {:.3f} * cos(\x)}}, {{8.35 * sin(\x)}});".format(x_scale / y_scale)
main += r"\draw [dashed, line width=0.3, color=gray, domain=-58:-92.5] plot ({{10.04 * {:.3f} * cos(\x)}}, {{10.04 * sin(\x)}});".format(x_scale / y_scale)

main += r'''
\draw[latex-, line width=1.4, color=cyan] (110.-120) to[bend left] (211.30);
\draw[-latex, line width=1.4, color=cyan] (110.-153) to[bend right] (211.60);

\draw[latex-, line width=1.4, color=cyan] (211.-3) to[bend right] (220.183);
\draw[-latex, line width=1.4, color=cyan] (211.3) to[bend left] (220.177);

\draw[latex-, dashed, line width=0.7] (211.-30) to[bend left] (321.130);
\draw[-latex, dashed, line width=0.7] (211.-40) to[bend right] (321.145);

\draw[latex-, line width=1.4, color=cyan] (220.-120) to[bend left] (321.30);
\draw[-latex, line width=1.4, color=cyan] (220.-153) to[bend right] (321.60);

\draw[latex-, line width=1.4, color=cyan] (321.-3) to[bend right] (330.183);
\draw[-latex, line width=0.7, color=magenta] (321.3) to[bend left] (330.177);

\draw[latex-, line width=1.4, color=cyan] (321.-120) to[bend left] (422.30);
\draw[-latex, line width=1.4, color=cyan] (321.-153) to[bend right] (422.60);

\draw[latex-, dashed, line width=0.7] (321.-30) to[bend left] (431.130);
\draw[-latex, dashed, line width=0.7] (321.-40) to[bend right] (431.145);

\draw[latex-, line width=1.4, color=cyan] (330.-120) to[bend left] (431.30);
\draw[-latex, line width=1.4, color=cyan] (330.-153) to[bend right] (431.60);

\draw[latex-|, line width=1.4] (422.-6) to[bend right] (431.186);
\draw[|-latex, line width=1.4] (422.6) to[bend left] (431.174);

\draw[latex-, line width=1.4, color=cyan] (431.-3) to[bend right] (440.183);
\draw[-latex, line width=0.7, color=magenta] (431.3) to[bend left] (440.177);

\draw[latex-, line width=0.8, color=magenta] (422.-35) to[bend left] (532.130);
\draw[-latex, line width=1.4, color=cyan] (422.-60) to[bend right] (532.150);

\draw[latex-, line width=1.4, color=cyan] (431.-120) to[bend left] (532.30);
\draw[-latex, line width=1.4, color=cyan] (431.-153) to[bend right] (532.60);

\draw[latex-, dashed, line width=0.7] (431.-30) to[bend left] (541.130);
\draw[-latex, dashed, line width=0.7] (431.-40) to[bend right] (541.145);

\draw[latex-, line width=1.4, color=cyan] (440.-120) to[bend left] (541.30);
\draw[-latex, line width=1.4, color=cyan] (440.-153) to[bend right] (541.60);

\draw[latex-|, line width=1.4] (532.-6) to[bend right] (541.186);
\draw[|-latex, line width=1.4] (532.6) to[bend left] (541.174);

\draw[latex-, line width=1.2, color=cyan] (541.-3) to[bend right] (550.183);
\draw[-latex, line width=0.7, color=magenta] (541.3) to[bend left] (550.177);

\draw[latex-, line width=1.4, color=cyan] (532.-120) to[bend left] (633.30);
\draw[-latex, line width=1.4, color=cyan] (532.-153) to[bend right] (633.60);

\draw[latex-, dashed, line width=0.7] (532.-30) to[bend left] (642.130);
\draw[-latex, dashed, line width=0.7] (532.-40) to[bend right] (642.145);

\draw[latex-, line width=1.4, color=cyan] (541.-120) to[bend left] (642.30);
\draw[-latex, line width=1.4, color=cyan] (541.-153) to[bend right] (642.60);

\draw[latex-, dashed, line width=0.7] (541.-30) to[bend left] (651.130);
\draw[-latex, dashed, line width=0.7] (541.-40) to[bend right] (651.145);

\draw[latex-, line width=1.4, color=cyan] (550.-120) to[bend left] (651.30);
\draw[-latex, line width=1.4, color=cyan] (550.-153) to[bend right] (651.60);

\draw[latex-|, line width=1.4] (633.-6) to[bend right] (642.186);
\draw[|-latex, line width=1.4] (633.6) to[bend left] (642.174);

\draw[latex-|, line width=1.4] (642.-6) to[bend right] (651.186);
\draw[|-latex, line width=1.4] (642.6) to[bend left] (651.174);

\draw[latex-, line width=1.4, color=cyan] (651.-3) to[bend right] (660.183);
\draw[-latex, line width=0.7, color=magenta] (651.3) to[bend left] (660.177);

\draw[latex-, line width=0.8, color=magenta] (633.-35) to[bend left] (743.130);
\draw[-latex, line width=1.4, color=cyan] (633.-60) to[bend right] (743.150);

\draw[latex-, line width=1.4, color=cyan] (642.-120) to[bend left] (743.30);
\draw[-latex, line width=1.4, color=cyan] (642.-153) to[bend right] (743.60);

\draw[latex-, dashed, line width=0.7] (642.-30) to[bend left] (752.130);
\draw[-latex, dashed, line width=0.7] (642.-40) to[bend right] (752.145);

\draw[latex-, line width=1.4, color=cyan] (651.-120) to[bend left] (752.30);
\draw[-latex, line width=1.4, color=cyan] (651.-153) to[bend right] (752.60);

\draw[latex-, dashed, line width=0.7] (651.-30) to[bend left] (761.130);
\draw[-latex, dashed, line width=0.7] (651.-40) to[bend right] (761.145);

\draw[latex-, line width=1.4, color=cyan] (660.-120) to[bend left] (761.30);
\draw[-latex, line width=1.4, color=cyan] (660.-153) to[bend right] (761.60);

\draw[latex-|, line width=1.4] (743.-6) to[bend right] (752.186);
\draw[|-latex, line width=1.4] (743.6) to[bend left] (752.174);

\draw[latex-|, line width=1.4] (752.-6) to[bend right] (761.186);
\draw[|-latex, line width=1.4] (752.6) to[bend left] (761.174);

\draw[latex-, line width=1.4, color=cyan] (761.-3) to[bend right] (770.183);
\draw[-latex, line width=0.7, color=magenta] (761.3) to[bend left] (770.177);

'''

content = header + main + footer

with open('network.tex','w') as f:
     f.write(content)

commandLine = subprocess.Popen(['pdflatex', 'network.tex'])
commandLine.communicate()

os.unlink('network.aux')
os.unlink('network.log')
