\chapter*{Summary}

Columnar structures refer to dense cylindrical packings of spheres.
They are ubiquitous throughout botany, foams, and have recently also become popular in nanoscience.
We investigate such structures by carrying out computer simulations, analytic calculations, as well as simple experiments.

Our simulations concern packings of \emph{soft} spheres of diameter $d$ inside a cylinder of diameter $D$.
We present a phase diagram of all stable structures, including so-called \emph{line-slip} arrangements, up to the diameter ratio $D/d = 2.71486$, where the nature of the densest sphere packing changes.
We also report on an experimental observation of such a line slip in a monodisperse foam confined in a cylinder.

Since macroscopic systems of this kind are not confined to the stable states of the phase diagram, we consequently explored structural transitions by carrying out further simulations.
Such transitions are in general hysteretic.
We summarise details of a reversible transition in form of a stability diagram.
All permissible structural transitions of stable packings from the phase diagram are then presented in a directed network.

Lee \emph{et al.} [T.~Lee, K.~Gizynski, and B.~Grzybowski, Adv. Mater. \textbf{29}, 1704274 (2017)] recently developed a novel method to assemble columnar structures inside a rotating cylinder.
In this method the spheres, which are of lower density than the surrounding fluids, are driven by a centripetal force toward the central axis.
Depending on the number of spheres, a variety of structures were observed.

We analysed this experimental method using analytic energy calculations for structures of soft spheres.
From these calculations, we obtained a phase diagram displaying interesting features including \emph{peritectoid} points.
These results are corroborated by numerical simulations for finite-sample size with soft spheres.
However, they also extend our previous results with the appearance of a line-slip structure due to finite-size effects.

The simplest columnar structure that can be assembled by Lee \emph{et al.}'s experimental method consists of a linear chain of spheres.
Such a chain, confined by a cylindrical harmonic potential, exhibits a variety of buckled arrangements as it is compressed longitudinally by hard walls at each ends.
We developed a theoretical model, based on an iterative stepwise method, as well as an analytic linear approximation to analyse this behaviour.
A wide range of predicted structures occurring via bifurcations are explored and presented in an energy bifurcation diagram.
The stable structures are also observed in experiments using rapid rotations and simulations based on energy minimisation.

Finally, we conclude all our results and present several directions this work may be extended in the future.
This includes the development of a new simulation model of such structures for soft matter such as soap bubbles or emulsion droplets.
We present the limits of the soft sphere model used in this thesis by comparing simulation results with those of an exact foam simulation.