\chapter{Rotational columnar structures of soft spheres}
\label{ch:rotational_structures}
\hrule
\vspace{8pt}
\textbf{Related publication:}
\vspace{-15pt}
\begin{enumerate}[noitemsep]
\item J. Winkelmann \emph{et al.} Phys Rev E \textbf{99} 020602(R) (2019).
\end{enumerate}
\vspace{-5pt}
\hrule
\vspace{8pt}

Recently, the subject of columnar structures has been given a new twist by the development of a novel experimental method by Lee \emph{et al.} \cite{lee2017non}.
In their experiments, the rapid spinning of a liquid-filled column containing spheres of lower density than the surrounding fluid drives the spheres toward the central axis.
Confined by a centripetal force, they assemble as columnar structures around this axis.

Lee \emph{et al.} performed lathe experiments on this assembly method by using polymeric beads, which are impenetrable and essentially behave like hard spheres.
Depending on the number of spheres and the rotational speed, they observed various types of structures.
Their experimental results are corroborated in parts with Molecular Dynamics (MD) simulations.

We adopt here a different approach using analytical methods to calculate the energy of such structures for \emph{soft} spheres.
From these we obtain a comprehensive phase diagram, of which the two axes correspond to a variation of the linear number density (number of spheres per unit length), which can be varied in an experiment, and the rotational frequency.
Finally, we compare our analytic results with soft sphere simulations of finite-system size to corroborate our calculation.
However, the finite-system size also introduces surprising modifications to the analytical phase diagram.

Lee \emph{et al.}'s simulations are computationally intensive because they describe the full motion of spheres inside a rotational flow, including inertia.
Our finite-size simulations, however, have the advantage of being computationally inexpensive.
They are based on energy minimisation and thus only describe the equilibrated columnar structure.

\section{Lee \emph{et al.}'s lathe experiments}
\label{sec:intro_rotation}

A new experimental method of realising columnar structures has recently been introduced by Lee \emph{et al.} \cite{lee2017non}.
We will summarise their work and results in this section.
They suspended monodisperse spheres inside a denser rotating fluid.
Rapid rotation of the system drives the spheres towards the axis of rotation where they assembled to different ordered structures.
Due to the rotation, the centripetal force confined the spheres inside a harmonic potential, which strength can be tuned by the rotational speed.

\begin{figure}[h]
\centering
\includegraphics[width=\textwidth]{rotcolumn.pdf}
\caption[Experimental set-up for rotational columnar structures]{
Experimental set-up for assembling columnar structures of spheres inside a rotating liquid-filled tube.
The liquid has a higher density than the spheres, making the spheres buoyant.
When the system is rotated with a rotational speed $\omega$, the spheres experience a centripetal force $\vec{F}_c$ pushing them towards the axis of rotation.
They assemble around this axis into different columnar structures.
[I reproduced the image after Lee \emph{et al.}'s experimental description \cite{lee2017non}.]
}
\label{fig:RotationSetUp}
\end{figure}

Lee \emph{et al.}'s experimental set-up is sketched in \figurename~\ref{fig:RotationSetUp}.
Polymeric beads of same diameter $d = \SI{1.588}{\milli\meter}$ and same density $\rho$ were placed in an aqueous solution of agarose and caesium bromide.
The density was varied for different experiments between $\rho = 0.9-\SI{1.13}{\gram\per\centi\meter}$.
By adding the caesium bromide, the density of the fluid was increased above the density of the beads, resulting in buoyancy of the spheres.
When rotated by a commercial lathe up to rotational speeds of $\SI{10000}{\rpm}$, a centripetal force pushes the beads toward the central axis and the liquid to the wall of the tube.
Because of a high Young's modulus of $E = \SI{1325}{\mega\pascal}$ \cite{designerdata}, these beads are impenetrable and behave like hard spheres.

Different structures were interconverted upon increasing or decreasing the rotation of the tube.
Additionally, the structure was determined by the number of polymeric beads inside the cylindrical tube at fixed length, i.e.\ the number density of the system.
With the agarose, the assembled structures were solidified by gelation and turned into permanent structures after the assembly process.
The assembly process was recorded with a high speed camera to investigate the structure before the solidification.

In order to gain further insides into the assembly process, Lee \emph{et al.} complemented their experiments with Molecular Dynamics (MD) simulations of (essentially) hard spheres.
These simulations describe the full motion of the spheres within a rotational flow.
Thus, they included inertial forces, drag forces in a viscous fluid, buoyant forces, and sphere--sphere interaction forces in their simulation, but no hydrodynamic interactions.

Depending on the rotational speed $\omega$ and the number of spheres, two different types of columnar structures were observed in experiment and simulation.
The structures were either homogeneous, i.e.\ a single phase structure filling out the entire length of the cylindrical tube, or a binary mixture, a mixture of two structures with an interface.
Two examples for each type of structure are shown in \figurename~\ref{fig:KoreanStructures}.
In \figurename~\ref{fig:KoreanStructures}(b) the interface of the binary structure is highlighted.

The existence of binary mixtures seems to be surprising on first sight because the Mermin--Wagner theorem \cite{Mermin1966} states that spontaneous symmetries cannot be broken for structures in one- and two-dimensions at finite temperature.
We showed in chapter \ref{ch:concept} that the types of columnar structure that we consider in this thesis can be reduced to two dimensions.
Due to the cylindrical confinement, the degrees of freedom of each sphere are only the height $z$ and azimuthal angle $\theta$.
This may lead to the conclusions that the Mermin--Wagner theorem prohibits binary mixtures in Lee \emph{et al.}'s experiments.
However, the structures in this chapter have three degrees of freedom: each sphere can potentially move in radial direction as well.
This allows the existence of binary mixtures where the spheres of each structure have different radial positions.

\begin{figure}[t]
\centering
\begin{subfigure}[t]{\textwidth}
\centering
\includegraphics[width=0.8\textwidth, clip=true, trim=0 160 0 165]{ExpPacking.png}
\caption{}
\end{subfigure}

\begin{subfigure}[t]{\textwidth}
\centering
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[width=0.8\textwidth]{mixed-structures.png}};

\draw[line width=1mm] (6.5, 1.9) ellipse (0.8cm and 1.9cm); 
\node[anchor=east, scale=1.5] at (5.7, 2.0) {interface};
\end{tikzpicture}

\caption{}
\end{subfigure}
\caption[Experimental examples of rotational columnar structures]{
Examples of observed columnar structures by Lee \emph{et al.} \cite{lee2017non} in lathe experiments involving rapid rotations.
Depending on the number of spheres and the rotational speed $\omega$, the polymeric beads assembled around the rotating axis either as homogeneous structures (a) or binary mixtures (b) of two homogeneous structures.
Homogeneous structures are single phase structures, filling out the whole tube.
A binary mixture consist of two structures with a visible interface, highlighted by the black ellipse in (b).
[Both images were extracted from Ref \cite{lee2017non}.]
}
\label{fig:KoreanStructures}
\end{figure}

For both, experiment and simulations, phase diagrams in terms of rotational speed vs number density were extracted (see \figurename~\ref{fig:lee_phasediagram}) \cite{lee2017non}.
The horizontal axes of the phase diagram consisted of the normalised particle number $n / n_0$.
The number $n$ corresponds to the number of polymeric beads in the tube and $n_0$ is the number of beads needed to form a linear chain inside the tube.
Thus, at $n / n_0 = 1$ the linear chain is observed.

Both phase diagrams resulted in the familiar sequence of columnar hard sphere packings:
The legend on the side of \figurename~\ref{fig:lee_phasediagram}(a) and (b) shows the same sequence of homogeneous structures as the uniform structures in \figurename~\ref{fig:hardspheresAdil}.
However, no intervening \enquote{line slips} were discovered (explanation for line slip in chapter  \ref{subsec:line-slip}).
Instead they report on mixed structures of two columnar hard sphere packings of different diameter ratios $D/d$.

\begin{figure}[h!]
\centering
\begin{subfigure}[t]{\textwidth}
\centering
\includegraphics[width=0.8\textwidth]{LeePhaseDiagram/Exp.pdf}
\caption{}
\end{subfigure}

\begin{subfigure}[t]{\textwidth}
\centering
\includegraphics[width=0.8\textwidth]{LeePhaseDiagram/Sim.pdf}
\caption{}
\end{subfigure}
\caption[Lee \emph{et al.}'s phase diagrams]{
Lee \emph{et al.}'s phase diagram constructed from experiments (a) and simulations (b) in terms of rotational speed (or rate) vs the normalised particle number $n / n_0$, which is a similar quantity to the number density.
$n$ is the number of spheres in the system and $n_0$ relates to the number of spheres that are needed to form a linear chain inside the tube. 
Homogeneous structures are marked with a single symbol (see legend on the side) and for mixtures the less dominant structures are indicated by the symbols in parentheses below.
[Both images are taken from Ref~\cite{lee2017non}.]
}
\label{fig:lee_phasediagram}
\end{figure}

Lee \emph{et al.} also reproduced all their experimentally observed structures with simulations.
However, comparing the experimental phase diagram (\figurename~\ref{fig:lee_phasediagram}(a)) with the one from the simulations (b), a shift towards lower number density can be observed for the experiments.
While this is not discussed in Ref \cite{lee2017non}, we think that possibly vibrations due to the spinning of the lathe might be the cause for this.
We experienced a similar shift in our analytic calculations to Lee \emph{et al.}'s experiments, which is discussed in section \ref{subsec:analytic_energy}.

Lee \emph{et al.} also discovered that the chirality (see chapter \ref{sec:structure_types}) for helical assembles can be controlled by the orientation of the axis of rotation of the tube with respect to gravity.
While experiments with horizontal tubes had no preferences for the chirality, in an inclined tube, the beads first localised at the top end.
During the rotation they then formed helices of the preferred handedness.
This effect was also reproduced in their simulations.

In further experiments columnar structures with bidisperse polymeric beads (of same density) were studied as well \cite{lee2017non}.
The larger polymeric beads assembled in a chain along the axis of rotation, while the smaller beads arranged around them.
The authors also presented preliminary results for beads assembled at the interface of two immiscible fluids.
These resembled structures of spheres assembled on the surface of a cylinder.
Finally, experiments of exploratory kind were also done with bubbles.
The reported findings only concerned simple linear chain structures, where the bubbles aligned along a straight line.

\section{Theory of columnar structures formed by rapid rotations}
\label{sec:theory_rotation}

Our approach here analyses Lee \emph{et al.}'s assembly method using the soft sphere model.
We will first focus on the rotational energies of hard sphere structures in section \ref{subsec:energies_hard_spheres}, which can be derived from previous findings.
The soft sphere model then allows us to analytically calculate the total energies for soft spheres in such a system (section \ref{subsec:analytic_energy}).
From these we deduct a phase diagram for columnar structures without internal spheres, which is rich in interesting features.

\subsection{Energies of hard sphere structures}
\label{subsec:energies_hard_spheres}

As described in chapter \ref{sec:densest_packing}, Mughal \textit{et al.}  \cite{mughal2011phyllotactic, mughal2012dense, mughal2013screw, mughal2014theory} have computed the densest columnar structures of hard spheres inside a cylinder.
While there is no mathematical proof of these results, they have been corroborated by others \cite{fu:2016wm, fu2016hard} and are in little doubt.
The packing fraction $\phi$ of the densest structures was computed as a function of the ratio $D/d$ of cylinder to sphere diameters (Fig. \ref{fig:hardspheresAdil} in chapter \ref{sec:densest_packing}).
Up to $D/d\approx 2.7$ these structures include only spheres in contact with the confining cylinder, so that all spheres are at the same distance $R$ from the central axis.
All of the homogeneous structures considered here are of this type.

\begin{figure}[t]
\begin{center}
\includegraphics[width=1.0\columnwidth]{hardsphere_results.jpg}
\caption[Minimal rotational energy for hard sphere packings]{
Minimal rotational energy $E_{\text{rot}}$ as a function of dimensionless inverse number density $(\rho d)^{-1}$ for \textit{hard sphere} packings.
The energy of the line-slip packings is given by the solid red line.
Vertical black lines indicate the location of homogeneous (in chapter \ref{ch:cylinder_packing} called uniform) structures, identified by indices $(l,m,n)$.
The \textit{straight} solid lines (green line) between adjacent homogeneous structures indicates that binary mixtures have a lower energy compared to line-slip packings.
The shaded area is the region of all possible hard sphere packings which have a single value of distance $R$ from the central axis, by definition this precludes binary mixtures.
The inset shows examples of a homogeneous and a line-slip structure.
See text for an interpretation of the two red arrows in the main figure.
}
\label{fig2}
\end{center}
\end{figure}

The densest hard sphere packings can be classified as either {\it homogeneous} (in chapter \ref{ch:concept} and \ref{ch:cylinder_packing} called uniform structure \cite{mughal2012dense}) packings or {\it line-slip} arrangements; examples of both are shown in the inset of Fig.~\ref{fig2}.
For a homogeneous arrangement, each sphere is in the same relation to six neighbouring spheres.
Such structures can be classified using the {\it phyllotactic} notation, i.e.\ a triplet of positive integers $(l=m+n,m,n)$ with $m\geq n$.
This notation is explained in detail in the introduction of chapter \ref{sec:phyllo}.

Intervening between these homogeneous packings, which are found at particular values of $D / d$, are line-slip structures in which contacts are lost along a line separating two spiral chains of the homogeneous structure as explained in detail in chapter \ref{subsec:line-slip}.

We can transform known hard sphere packing results to give us the lowest (rotational) energy per sphere (that is, the minimal $R$) as a function of the dimensionless inverse number density $(\rho d)^{-1}$ (see \figurename~\ref{fig2}), as follows.
If the rotational velocity is $\omega$, then by the parallel axis theorem, the rotational energy per sphere is
\begin{equation}
E_{\text{rot}}= \frac{1}{2} \omega^2 (I_0 + M R^2)\,,
\end{equation}
where $I_0 = Md^2 / 10$ is the moment of inertia of a sphere with mass $M$ and sphere diameter $d$.
Since the moment of inertia of a sphere is independent of the distance from the axis $R$, we will omit this term in the calculations below.
For hard sphere packings the sphere centres are located at a distance $R =(D-d)/2$ from the cylindrical axis, thus the rotational energy for hard sphere packings is given by
\begin{equation}
E^{\mathrm{H}}_{\text{rot}} = \frac{1} {8} \omega^2 M (D - d)^2\,.
\end{equation}
The dimensionless inverse number density $(\rho d)^{-1}$ can be computed from the diameter ratio and the packing fraction $\phi$.

For a finite system of length $L$ and number of spheres $N$, $\phi$ is given by
\begin{equation}
\phi = \frac{4 N V_{\text{Sphere}}}{\pi D^2 L} = \frac{2}{3} \frac{N d}{L} \frac{d^2}{D^2}\,,
\label{e:phi_number_dens}
\end{equation}
where $V_{\text{Sphere}}$ is the volume of a sphere.
In the limit of an infinite system ($N \to \infty$ and $L \to \infty$), the number density $\rho$ is introduced $Nd / L \to \rho$.
Rearranging eq \ref{e:phi_number_dens} for the inverse number density, we then get
\begin{equation}
(\rho d)^{-1} = \frac{2}{3} \left(\frac{d}{D}\right)^2 \phi^{-1}\,.
\end{equation}

%In \figurename~\ref{fig2} we plotted the transformed rotational energy against the calculated inverse number density $(\rho d)^{-1}$.
\figurename~\ref{fig2} implies that the homogeneous structures, which occur for special values of $(\rho d)^{-1}$, minimise the rotational energy per sphere and we expect to observe this type of structure at these values of $(\rho d)^{-1}$.
In the intervening ranges, however, binary mixtures (consisting of two-phase structures) of the adjacent homogeneous structures are expected.
The energies of the binary mixtures, dictated by the usual Maxwell (common tangent) construction, lies below the line-slip energies in \figurename~\ref{fig2}.

For the homogeneous structures as well as line-slip arrangements, the packing inside the cylindrical channel is filled with a single structure.
Thus, these structures do not need to accommodate an interface.
The binary mixture, however, consists of two structures that differ in their $D/d$ and therefore has an interface.
While the Maxwell construction predicts the energy for an infinite system and neglects this interface energy, for finite-system sizes this interface energy plays an important role (see section {\ref{sec:finite-size}}).

Note that the hard sphere limit may be approached in two ways as indicated by the red arrows in \figurename~\ref{fig2}:
Following the horizontal arrow, we approach the hard sphere limit by a volumetric change, as done in the packing simulation of chapter \ref{ch:cylinder_packing}.
This approach is essentially described by reducing the pressure in the phase diagram of \figurename~\ref{fig:phasediagram1}.
The vertical approach to the hard sphere limit is achieved by changing the rotational speed, as in this chapter.
We will discuss the phase diagram related to the vertical red arrow in section \ref{subsec:analytic_energy}.

The resultant structure sequence is in accord with the findings of Lee \textit{et al.} \cite{lee2017non}.
In the following we address its modification in the case of soft spheres and finite column length.

\subsection{Analytic soft sphere energy calculations}
\label{subsec:analytic_energy}

The effect of moving away from the hard sphere limit is to widen the range in which the homogeneous $(l,m,n)$ structures are found.
This can be quantified and understood in terms of a transparent analytical description, illustrated by \figurename~\ref{fig:energy} and described below.

For a rotating column of soft spheres the energy per sphere is given by
\begin{equation}
E_{\text{rot}}^{\mathrm{S}}=E_{\text{rot}}+E_{o}\,,
\end{equation}
where the second term is due to the sphere--sphere interaction.
We use the same soft sphere interaction between sphere $i$ and $j$ as described in chapter \ref{subsec:soft_spheres}, namely
\begin{equation}
E_{ij} = \begin{cases} 0 \quad& \delta_{ij} > 0 \\ \frac{1}{2} k \delta_{ij}^2 \quad& \delta_{ij} \le 0
\end{cases}
\end{equation}
where $k$ is the spring constant.
The overlap $\delta_{ij}=\abs{\vec{r}_i-\vec{r}_j}-d$ depends on the sphere positions $\vec{r}_i$ and $\vec{r}_j$.

The total overlap energy per sphere $E_o$ can be written as
\begin{align}
E_{o} &= \frac{1}{2} \frac{k}{N} \sum_{\substack{i, j = 0 \\ i \neq j}}^N \delta^2_{ij} \nonumber\\
&= \frac{1}{2}k\avg{\delta^2_{ij}} \,,
\end{align}
i.e.\ it is obtained by summing over all pairwise interactions and dividing by the total number of spheres in the structure.
Thus the energy per sphere is given by,
\begin{equation}
\frac{E_{\text{rot}}^{\mathrm{S}}}{M \omega^2 d^2} = \frac{1}{2} \frac{R^2}{d^2} + \frac{1}{2} \frac{k}{M \omega^2} \avg{\left(\frac{\delta_{ij}}{d}\right)^2}\,.
\label{eq:totenergy}
\end{equation}

Homogeneous structures are comprised of packings for which each sphere is in an identical relationship to every other sphere in the packing. Each sphere is at a distance $R$ from the central axis and is in contact with six neighbouring spheres.
From these constraints, it follows that for a given number density $\rho$, the energy of a homogeneous structure can be varied \emph{only} by a uniform radial compression/expansion or twist, if it is to remain homogeneous (as we assume here).
Thus the energy per sphere for all homogeneous structures is an analytic expression to be minimised with respect to only two variables: $R$ and a twist angle $\alpha$ \cite{mughal2012dense}.
In the case of achiral structures this twist angle is zero due to symmetry, and only one free variable remains.

The detailed calculation for the analytic energy expression of all homogeneous and achiral $(l, l, 0)$ structures (i.e.\ $(2, 2, 0)$, $(3, 3, 0)$, $(4, 4, 0)$, etc.) is given in Appendix \ref{sec:appendix_analytic}.
The minimal energy can either be found by a root search of the gradient or using a numerical minimisation routine for scalar functions.

The minimised energies of all homogeneous structures as a function of the inverse number density $(\rho d)^{-1}$ are shown for two different values of $\omega^2 M / k$ in \figurename~\ref{fig:energy}.
These calculations are based on the full harmonic interactions between neighbours (see below for justification).
The common tangents between adjacent curves are shown by the black lines and the points of contact between the tangent and the curve by black dots. Where the common tangents are below the energy curves of the homogeneous structures, the binary mixtures are more stable. For the other values of $(\rho d)^{-1}$, highlighted by the shaded strips, a homogeneous phase is predicted. Low values of $\omega^2 M / k$ correspond to the hard sphere limit (see \figurename~\ref{fig:energy}(b)); with increasing $\omega^2 M / k$ the overlap between spheres increases, resulting in a broadening of the range over which homogeneous structures are observed.

There is no loss of contacts in the range in which homogeneous structures are predicted.
This justifies the simplification that resulted from not taking loss of contacts into account, i.e.\ using the full harmonic approximation of interactions.

\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.91\textwidth]{Energies.pdf}
\caption[Analytically calculated minimal rotational energy]{
Minimal energy per sphere for all homogeneous structures as a function of dimensionless inverse number density $(\rho d)^{-1}$ for the case of \textit{soft spheres}, with harmonic interactions (see text).
(a) shows the result for $\omega^2 M / k = 0.55$ and (b) for $\omega^2 M / k = 0.10$, which is close to the hard sphere limit.
Common tangents between adjacent homogeneous structures are shown by the black lines and the black dots are the tangent points.
The ranges over which homogeneous structures are expected, are highlighted by shaded strips (coloured according to the appropriate homogeneous structure - see key).
Outside these ranges the common tangents have a lower energy and binary mixtures are expected.
}
\label{fig:energy}
\end{center}
\end{figure}

The corresponding analytically calculated phase diagram
\interfootnotelinepenalty=10000
\footnote{
The phase diagram was created by calculating the analytic energy for each homogeneous structure first and then finding each common tangent by a matching slope algorithm at a certain $\omega^2 / M k$ value.
The points where the tangents meet the energy curves of the homogeneous structures are the borders of the coloured regions in the phase diagram.
The resolution in $\omega^2 / M k$ is $\num{0.01}$.
}
is shown in \figurename~\ref{fig:phasediagram2}.
In the hard sphere limit ($\omega^2 M / k \rightarrow 0$), the values of $(\rho d)^{-1}$ for the homogeneous structures are consistent with those from the simulations of Lee \textit{et al.} [\figurename~\ref{fig:lee_phasediagram}(b)] which we have indicated by points with dashed horizontal error bars.
Lee \emph{et al.}'s experimental data are shifted toward higher values of $(\rho d)^{-1}$.
This is possibly due to vibrations, keeping the hard spheres slightly apart, which gives them an effective diameter larger than their actual size.
We report on a similar effect in experiments of the next chapter \ref{sec:comparison_exp}.

\begin{figure}[h!]
\begin{center}
\includegraphics[width=0.9\textwidth]{phasediagram.pdf}
\caption[Phase diagram of rotational columnar structures]{
Calculated phase diagram of homogeneous structures (coloured, labelled regions) and their binary mixtures of adjacent homogeneous structures (intervening white space).
Most homogeneous regions expand with increasing $\omega^2 M / k$ (i.e.\ rotation frequency) but the achiral $(3, 3, 0)$ and $(4, 4, 0)$ structures vanish in \textit{peritectoid} points (see the inset) \cite{peritectoid}.
In the case of the $(5,5,0)$ phase only the right hand boundary can be shown, since it is the last structure without inner spheres.
The $(\rho d)^{-1}$ values for the homogeneous structures are in good agreement with Lee \emph{et al.}'s simulation results \cite{lee2017non}, indicated by points with dashed horizontal error bars in the hard sphere limit.
}
\label{fig:phasediagram2}
\end{center}
\end{figure}

With increasing $\omega^2 M / k$ the ranges of homogeneous structures expand, as expected.
The upper part of the phase diagram is, however, much richer in detail than anticipated.
Of special interest is the vanishing of the homogeneous achiral structures $(3, 3, 0)$ and $(4, 4, 0)$ at a rotational velocity of $\omega^2 M / k \approx 0.5$. For high $\omega^2 M / k$ the achiral structures cannot compete with chiral structures: The latter can deform by twisting, while the former cannot.

At the values of $\omega^2 M / k$ where these achiral structures disappear, there are \textit{peritectoid} points (see inset of \figurename~\ref{fig:phasediagram2}) \cite{peritectoid}.
The homogeneous structures vanish in a point and also their two adjacent binary mixtures disappear.
Above these three structure a new binary mixture of the second nearest homogeneous structures emerges. 
The phase boundaries of the adjacent homogeneous structures show a change in slope where the new binary mixture appears.
This is due to the change of the common tangent, now to be taken between the second-nearest homogeneous structures.

Note that a peritectoid transformation actually describes a type of isothermal reversible reaction from metallurgy, in which two solid phases react with each to create an alloy that is a completely different solid phase.
Our structure do not undergo any thermal reaction (since we do not have a thermal system).
We still apply this term from metallurgy to our phase transitions because both share the same topological features in the phase diagram.

\section{Finite-system size simulations of columnar structures by rapid rotations}
\label{sec:finite-size}

The analytic results, however, are only valid for systems of infinite sample size.
In order to corroborate our analytical results, but also extend them to investigations of finite-system sizes, we carried out finite-size simulations based on energy minimisation.
This simulation model is explained in detail in the next section \ref{subsec:energy_minimisation}.
These lead to the unexpected discovery of a line-slip arrangement in such a finite-size system.
We present these results in form of a modified phase diagram (compared to \figurename~\ref{fig:phasediagram2}) in section \ref{subsec:line-slips_finite}.

\subsection{The simulation model: Energy minimisation}
\label{subsec:energy_minimisation}

We slightly modified the soft sphere simulation model described in \ref{sec:model} in order to simulate columnar structures of finite size assembled by rapid rotations.
It can be used as more general numerical simulations for a finite system of $N$ spheres which can occupy any position in a unit cell of length $L$ (see \figurename~\ref{fig:rot_model}).

\begin{figure}[h]
\centering
\begin{tikzpicture}
\node[anchor=south west,inner sep=0] (image) at (0,0) {
\includegraphics[width=0.5\textwidth, clip=True, trim=250 0 250 0]{321packing.png}};
\begin{scope}[x={(image.south east)},y={(image.north west)}]
\draw[latex-, line width=1] (0.38, 0.82) -- (0.05, 0.71) node[below, text width=2.5cm, scale=1.7] {image spheres};
\draw[-latex, line width=1] (0.05, 0.4) -- (0.38, 0.23);
\draw (0.7, 0.66) -- (0.9, 0.66); 
\draw (0.7, 0.36) -- (0.9, 0.36); 
\draw[latex-latex, line width=1] (0.8, 0.66) -- node[right] {$L$} (0.8, 0.36);
\draw (0.55, 0.289) -- (0.9, 0.289);
\draw (0.55, 0.057) -- (0.9, 0.057);
\draw[latex-latex, line width=1] (0.8, 0.057) -- node[right] {$d$} (0.8, 0.289); 
\draw[line width=1pt] (0.50, 0.6) -- (0.50, 1.0) node {\AxisRotator[rotate=-90]};
\node[scale=1.8] at (0.38, 1.03) {$\omega$};
\draw[line width=1pt] (0.50, 0.1) -- (0.50, -0.11);
\draw[line width=3pt] (0.27, 0.15) parabola bend (0.5, -0.1) (0.73, 0.15);
\draw[line width=1pt] (0.28, -0.1) -- (0.72, -0.1);
\node[below right, scale=1.4] at (-0.15, 0.05) {$E_{\text{rot}} \propto \omega^2 R^2$};
\end{scope}
\end{tikzpicture}
\caption[Set-up for finite-size simulation of rotational columnar structures]{
Simulation of $N$ soft (overlapping) spheres (\textcolor{blue}{blue}) with diameter $d$ confined by a rotational energy $E_{\text{rot}}$ in a unit cell of fixed length $L$.
The spheres can now occupy any position within this unit cell.
The energy $E_{\text{rot}}$ is proportional to the rotational speed $\omega^2$ and the radial distance from the central axis $R^2$.
Periodic boundaries are again employed by image spheres (\textcolor{red}{red}) above and below the unit cell that are rotated by a twist angle $\alpha$.
}
\label{fig:rot_model}
\end{figure}

In doing so, we minimise the total energy per sphere
\begin{equation}
\frac{E(\{\vec{r}_i\}, \alpha)}{M \omega^2 d^2} = \frac{1}{2N} \sum_{i}^N \left(\frac{R_i}{d}\right)^2 + \frac{1}{2} \frac{k}{N 
M\omega^2} \sum_{i = 1}^N \sum_{j=i}^N \frac{|\vec{r}_i - \vec{r}_j|^2}{d^2}
\end{equation}
with respect to all sphere positions $\vec{r}_i$ and twist angle $\alpha$ at fixed unit cell length $L$.
For the rotational energy term we sum over all radial distances $R_i^2$ of sphere $i$ and the interaction energy is again that of soft spheres.
We have also applied twisted boundary conditions using image spheres above and below the unit cell as illustrated in \figurename~\ref{fig:rot_model} \cite{mughal2012dense} (see also chapter \ref{sec:model}).

We use the Basin-hopping method \cite{basinhopping} to search for the global minimal energy for particular values of number density $\rho = N / L$ and $\omega^2 M / k$.
It is introduced in chapter \ref{subsec:global_minimum} and described in detail in Appendix \ref{sec:appendix_minimisation}.

We explore structures with low number densities between $2 < \rho d < 3$ ($0.3 < (\rho d)^{-1} < 0.5$ for the inverse number density).
The number density $\rho d$ can be varied by fixing the sphere number $N$ and adjusting the unit cell size $L$. 
From the hard sphere results we know that the only possible structures in this regime are the homogeneous phases $(2, 1, 1)$, $(2, 2, 0)$ and $(3, 2, 1)$ and their corresponding line slips.
Finite values of $N$ which are multiples of $12$ are compatible with these structures. 
Thus, in the finite-size simulations presented here we have used $N = 24$.

\subsection{Appearance of a line slip due to finite-size effects}
\label{subsec:line-slips_finite}

\begin{figure}
\begin{center}
\includegraphics[width=0.8\columnwidth ]{EnergiesSim.pdf}
\caption[Energies from finite-size simulations]{
The red dots show the energy from finite-size simulations, for $\omega^2 M / k = 0.2$ and $N = 24$. 
Brown and blue solid lines are the previously analytically calculated energies for the labelled homogeneous structures, and the common tangents (black solid lines) represent their binary mixture.
Within the vertical dashed red and brown line, a line-slip structure is observed.
At the vertical dashed blue line the $(3, 2, 1)$ transforms into a binary mixture of a homogeneous and line-slip structure, whose energy is higher than that of the common tangent (due to finite-size effects).
}
\label{fig:EnergiesSim}

\includegraphics[width=0.8\columnwidth ]{SimulationPhaseDiagram.pdf}
\caption[Phase diagram of finite-size simulations]{
Numerically computed phase diagram from the finite-size simulation with $N = 24$ spheres. 
In addition to the expected homogeneous structures and binary mixture of $(2, 2, 0)$ and $(2, 1, 1)$, a line-slip arrangement, as well as its binary mixture with $(3, 2, 1)$ are found.}
\label{fig:SimPhase}
\end{center}
\end{figure}

We first compared our analytically calculated energies with energies from the finite-size simulations.
An example of our numerical results for a low rotational velocity (i.e.\, $\omega^2 M/k=0.2$) is shown in \figurename~\ref{fig:EnergiesSim}.
Here we explore the region between the $(2, 2, 0)$ and $(3, 2, 1)$ homogeneous soft sphere structures.
The blue and brown solid curves are their analytically computed energies; the solid black line is the common tangent between them.
The numerically computed energy from the finite-size simulations, shown by the red dotted line, closely matches the analytic theory within the ranges of the homogeneous phases.

However, a notable difference arises for $0.3205 \le (\rho d)^{-1} \le 0.3520$.
Here, the computed energy is slightly higher than that of the common tangent (binary mixture) due to finite-size effects.
Between the dashed vertical blue [$(\rho d)^{-1} = 0.3205$] and red lines [$(\rho d)^{-1} = 0.3333$] we find a mixture of the $(3, 2, 1)$ uniform structure and a $(2, \bm{2}, 0)$ line slip.
Between the dashed vertical red [$(\rho d)^{-1} = 0.3333$] and the brown lines [$(\rho d)^{-1} = 0.3520$] we observe only the line-slip structure.

In part these results corroborate those of the analytic treatment -- in particular in regards to the homogeneous phases -- but the intervention of the line slip was an unexpected effect of finite size.
In these finite systems the interface energy of the binary mixtures has a finite contribution to the total energy, which decreases with increasing number of spheres.
Results for the range in number density presented here, using $N = 48$ and $96$, indicate that the binary mixture of two homogeneous phases is recovered for infinite system size.
The energy per sphere for these system sizes approaches that of the common tangent.

From our finite-size simulations we finally compute a limited phase diagram, shown in \figurename~\ref{fig:SimPhase}, to be compared with \figurename~\ref{fig:phasediagram2}.
In the case of the $(2,1,1)$ and $(2,2,0)$ structures the intervening region is occupied as expected by the $(2,2,0)-(2,1,1)$ mixed phase structure.
However, in the case of the $(2,2,0)$ and $(3,2,1)$ structures, the intervening region is split into two parts, featuring the line slip mentioned above for low values of $\omega^2 M / k$.
%It is to be expected that line slips will play a role in all of the other parts of this phase diagram, in finite simulations.

\section{Conclusions}
The phase diagram presented in \figurename~\ref{fig:phasediagram2} provides an analytic guide to the expected occurrence of equilibrium structures in long rotating columns on the basis of a generic soft sphere model.
It is packed with great features, such as the two peritectoid points, at which two achiral structures are predicted to vanish.

We have adduced results from more general simulations, as well.
In part these results corroborate those of the analytic treatment -- in particular as regards the homogeneous phases -- but the intervention of the line slip was an unexpected effect of finite size.
It is to be expected that line slips will play a role in all the other parts of the phase diagram, in simulations of finite-system size.

The wide range of other unexpected structures remains to be explored for future work, as well as a rigorous analysis of the asymptotic trend as $N$ goes to infinity.  
Results for the line slip investigated here indicate that the binary mixture of two homogeneous phases is recovered in that limit.
There also remains the case of hard wall boundary conditions at both ends of the tube in a finite sample, which is more directly relevant to the present experiments.
The general simulations from this chapter can easily be modified in this direction by replacing the periodic boundaries with a wall potential at both ends.

The interesting feature of peritectoid points in the phase diagram has also not yet been explored in simulations and experiments.
Since these points were discovered in the analytic calculations, they may only be features of infinite systems.
Their relevance for finite system can be studied with the simulations introduced in this chapter.
The experiments will have to be performed with spherical objects that are deformable because peritectoid points are features of soft systems.
We will discuss possible future experiments of this kind using hydrogel spheres in the outlook of chapter \ref{ch:outlook}.

The finite-size effect of line slips should also be investigated in future experiments.
Since we expect this finite-size effect also in the hard sphere limit, these experiments can conveniently be done with polymeric beads, as already used in experiments by Lee \emph{et al.}
In any such experiments, one should also be aware of the existence of metastability and hysteresis in macroscopic systems, which we explored in chapter \ref{sec:hysteresis} in a related context.
All possible future investigations to this topic using simulations, as well as experiments will be discussed in detail in the outlook of chapter \ref{ch:outlook}.